import {TrafficHandler} from './TrafficHandler';

const directions =['North','East','South','West'];
let trafficHandler = new TrafficHandler();

trafficHandler.on('vehicle', (direction:string) =>{
    console.log('Vehicles coming from ', direction, ' direction are moving');
});

trafficHandler.on('signal', (direction:string) => {
    console.log("-----------------------------------------------------")
    console.log('Signal is green for vehicles coming from ', direction, ' direction');
    trafficHandler.emit('vehicle', direction);
});

let count = 0;

function simulateTraffic() {
    trafficHandler.emitSignalEvent(directions[count++ %4]);
}

setInterval(simulateTraffic, 5000); // 5 seconds of green signal for each direction