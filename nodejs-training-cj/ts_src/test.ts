import {Singleton} from './singleton_pattern'

let singletonObject = Singleton.getInstance();
console.log("calling getInstance first time");
console.log(singletonObject);

singletonObject.doubleIt();

console.log("calling getInstance second time")
singletonObject = Singleton.getInstance();
console.log("we get the same object that we just modified", singletonObject);