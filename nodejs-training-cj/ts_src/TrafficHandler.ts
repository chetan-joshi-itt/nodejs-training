const EventEmitter = require('events');

export class TrafficHandler extends EventEmitter {

    emitSignalEvent(direction : string) {
        this.emit('signal', direction);
    }

    emitVehicleEvent(direction : string) {
        this.emit('vehicle', direction);
    }
}