const fs = require('fs');

var dataToWrite = 'Writing some data to myNewFile...';

var data = fs.readFileSync(__dirname+'/data_files/data.txt', 'utf-8');
console.log("data read from file: ", data);

fs.appendFileSync(__dirname+'/data_files/myNewFile.txt', dataToWrite);