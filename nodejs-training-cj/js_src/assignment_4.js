
const fs = require('fs');

let studentDepartmentData = {};
function readFile(filepath, arrayName) {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, 'utf-8', (error, data)=> {
            if (error) {
                console.log(error);
                reject (error);
            }
            studentDepartmentData[arrayName] = JSON.parse(data)[arrayName];
            resolve('success');
        });
    });
}

let promises = [];
promises.push(readFile(__dirname+'/data_files/student.txt', 'students'));
promises.push(readFile(__dirname+'/data_files/departments.txt', 'departments'));

Promise.all(promises).then((status) => {
    if (status!='success'){
        console.log('error reading files..');
        return;
    }
    let students = {};
    let departments = {};

    studentDepartmentData.students.forEach((value, i) => {
        students[value.studentId] = value.stuName;
    });
    studentDepartmentData.departments.forEach((value, i) => {
        departments[value.departmentId] = value.deptName;
    });

    fs.readFile(__dirname+'/data_files/studentDepartmentMap.txt','utf-8', (err, data) => {
        if(err) {
            throw err;
        }
   
        let mappingData = JSON.parse(data).studDeptMap;
        let result = {};

        mappingData.forEach((mapping, i) => {
            let deptName = departments[mapping.departmentId];
            let studentName = students[mapping.studentId];

            if (!result[deptName]) {
                result[deptName] = [];
            }
            let values = result[deptName];
            values.push(studentName);
            result[deptName] = values;
        });

        fs.writeFile(__dirname+'/data_files/assignment_4_result.txt', JSON.stringify(result), (err) =>{
            if (err) {
                throw err;
            }
            console.log(JSON.stringify(result)+"\n result is written to file");
        });
    });
});