const fs = require('fs');

const data = [{'name':'John','age':'24'}, {'name':'Monica','age':'26'}];

let convertToText = function(jsonArray) {
    let keys = [];

    for(key in jsonArray[0]) {
        keys.push(key);
    }
    let textData = keys.join();

    jsonArray.forEach(jsonObject => {
        let values = [];

        for(key in jsonObject) {
            values.push(jsonObject[key]);
        }

        textData += "\n";
        textData += values.join();
    });

    return textData;
}

let textData = convertToText(data);

fs.appendFile(__dirname+'/data_files/assignment2.txt', textData, (err)=> {
    if (err)
        throw err;
    console.log("successfully written data to the file");
});
