const fs = require('fs');

console.log('file content before truncate', fs.readFileSync(__dirname+'/data_files/assignment2.3.txt', 'utf-8'));
fs.truncate(__dirname+'/data_files/assignment2.3.txt', 5, (err)=> {
    fs.readFile(__dirname+'/data_files/assignment2.3.txt', 'utf-8', (err, data)=> {
        if (err)
            throw err;
        console.log('file content after truncate: ', data);
    });
})