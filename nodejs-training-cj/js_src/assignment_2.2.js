// read data from file and convert to array format

const lineReader = require('readline').createInterface({
    input : require('fs').createReadStream(__dirname+'/data_files/assignment2.txt')
});

let lines = [];
lineReader.on('line', (line,arg) => {
    lines.push(line);
});
let jsonArray = [];

lineReader.on('close', ()=>{
    var keys = lines[0].split(',');

    let jsonArray = [];
    lines.forEach((line, index)=>{
        let jsonObject = {};
        if(index > 0){
           values = line.split(',');
           for (let i=0; i<keys.length; i++) {
            jsonObject[keys[i]] = values[i];
           }
           jsonArray.push(jsonObject);
        }
    });
    console.log("converted text content from file to json array:\n", jsonArray);
});