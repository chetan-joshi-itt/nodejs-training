"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var singleton_pattern_1 = require("./singleton_pattern");
var singletonObject = singleton_pattern_1.Singleton.getInstance();
console.log("calling getInstance first time");
console.log(singletonObject);
singletonObject.doubleIt();
console.log("calling getInstance second time");
singletonObject = singleton_pattern_1.Singleton.getInstance();
console.log("we get the same object that we just modified", singletonObject);
//# sourceMappingURL=E:/Node js/nodejs-training/nodejs-training-cj/sourcemap_files/test.js.map