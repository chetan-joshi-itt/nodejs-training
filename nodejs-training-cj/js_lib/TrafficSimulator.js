"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TrafficHandler_1 = require("./TrafficHandler");
var directions = ['North', 'East', 'South', 'West'];
var trafficHandler = new TrafficHandler_1.TrafficHandler();
trafficHandler.on('vehicle', function (direction) {
    console.log('Vehicles coming from ', direction, ' direction are moving');
});
trafficHandler.on('signal', function (direction) {
    console.log("-----------------------------------------------------");
    console.log('Signal is green for vehicles coming from ', direction, ' direction');
    trafficHandler.emit('vehicle', direction);
});
var count = 0;
function simulateTraffic() {
    trafficHandler.emitSignalEvent(directions[count++ % 4]);
}
setInterval(simulateTraffic, 5000); // 5 seconds of green signal for each direction
//# sourceMappingURL=TrafficSimulator.js.map