"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EventEmitter = require('events');
var TrafficHandler = /** @class */ (function (_super) {
    __extends(TrafficHandler, _super);
    function TrafficHandler() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TrafficHandler.prototype.emitSignalEvent = function (direction) {
        this.emit('signal', direction);
    };
    TrafficHandler.prototype.emitVehicleEvent = function (direction) {
        this.emit('vehicle', direction);
    };
    return TrafficHandler;
}(EventEmitter));
exports.TrafficHandler = TrafficHandler;
//# sourceMappingURL=TrafficHandler.js.map